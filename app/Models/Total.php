<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Total extends Model
{
    use HasFactory;

    protected $fillable = [
        "rest",
        "arrival",
        "open",
        "open_desc",
        "end_desc",
    ];

    public static function getTotalByDay($date = null)
    {
        return self::when($date, function($q) use ($date) {
            return $q->whereDate('created_at', $date);
        }, function($q) use ($date) {
            return $q->whereDate('created_at', Carbon::today());
        })->first() ?? null;
    }

    public static function getTotalByRange($date = [])
    {
        return (array)\DB::table('totals')->selectRaw('sum(totals.rest) as rest, sum(totals.arrival) as arrival, sum(totals.open) as open')
            ->whereDate('created_at', '>=', $date[0])
            ->whereDate('created_at', '<=', $date[1])
            ->get()->toArray()[0];
    }
}

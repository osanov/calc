<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'city_id',
    ];

    public function expenses()
    {
        return $this->hasMany(Expense::class, 'project_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}

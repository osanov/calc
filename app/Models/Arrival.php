<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Arrival extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'value',
        'arrival_type_id',
    ];

    public function arrivalType()
    {
        return $this->belongsTo(ArrivalType::class, 'arrival_type_id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTotalRequest;
use App\Http\Requests\UpdateTotalRequest;
use Illuminate\Http\Request;
use App\Models\Total;
use Carbon\Carbon;

class TotalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($date = $request->get('date')) {
            $total = count($date) > 1 ? Total::getTotalByRange($date) :Total::getTotalByDay($date);
            if ($total) {
                return response($total);
            }
            return response()->json([
                'rest' => 0,
                'arrival' => 0,
                'open' => 0,
            ]);
        } else {
            return response(Total::getTotalByDay());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTotalRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTotalRequest $request)
    {
        return response(Total::create($request->post()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Total  $total
     * @return \Illuminate\Http\Response
     */
    public function show(Total $total)
    {
        return response($total);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTotalRequest  $request
     * @param  \App\Models\Total  $total
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTotalRequest $request, Total $total)
    {
        return response($total->update($request->post()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Total  $total
     * @return \Illuminate\Http\Response
     */
    public function destroy(Total $total)
    {
        $total->delete();
        return response()->json(['status' => 'ok']);
    }


    public function updateTotal(Request $request)
    {
        $total = Total::when($request->has('date'), function($q) {
            return $q->whereDate('created_at', request()->get('date'));
        }, function($q) {
            return $q->whereDate('created_at', Carbon::today());
        })->first();
        if ($total) {
            $total->rest = $request->get('rest');
            $total->save();
            return response($total);
        } else {
            $total = Total::create();
            $total->rest = $request->get('rest');
            $total->save();
            return response($total);
        }
    }

    public function getTotal(Request $request)
    {
        $total = Total::when($request->has('date'), function($q) {
            return $q->whereDate('created_at', request()->get('date'));
        }, function($q) {
            return $q->whereDate('created_at', Carbon::today());
        })->first();
        if ($total) {
            return response($total);
        } else {
            $total = Total::create();
            $total->save();
            return response($total);
        }
    }

    public function addTotal(Request $request)
    {
        $total = Total::when($request->has('date'), function($q) {
            return $q->whereDate('created_at', request()->get('date'));
        }, function($q) {
            return $q->whereDate('created_at', Carbon::today());
        })->first();
        if ($total) {
            $total->rest += $request->get('rest');
            $total->save();
            return response($total);
        } else {
            $total = Total::create();
            $total->created_at = request()->get('date');
            $total->rest += $request->get('rest');
            $total->save();
            return response($total);
        }
    }

    public function subTotal(Request $request)
    {
        $total = Total::when($request->has('date'), function($q) {
            return $q->whereDate('created_at', request()->get('date'));
        }, function($q) {
            return $q->whereDate('created_at', Carbon::today());
        })->first();
        if ($total) {
            $total->rest -= $request->get('rest');
            $total->save();
            return response($total);
        } else {
            $total = Total::create();
            $total->created_at = request()->get('date');
            $total->rest -= $request->get('rest');
            $total->save();
            return response($total);
        }
    }

    public function updateOpenDesc(Request $request)
    {
        $total = Total::when($request->has('date'), function($q) {
            return $q->whereDate('created_at', request()->get('date'));
        }, function($q) {
            return $q->whereDate('created_at', Carbon::today());
        })->first();
        if ($total) {
            $total->open_desc = $request->get('open_desc');
            $total->save();
            return response($total);
        } else {
            $total = Total::create();
            $total->created_at = request()->get('date');
            $total->open_desc = $request->get('open_desc');
            $total->save();
            return response($total);
        }
    }

    public function updateEndDesc(Request $request)
    {
        $total = Total::when($request->has('date'), function($q) {
            return $q->whereDate('created_at', request()->get('date'));
        }, function($q) {
            return $q->whereDate('created_at', Carbon::today());
        })->first();
        if ($total) {
            $total->end_desc = $request->get('end_desc');
            $total->save();
            return response($total);
        } else {
            $total = Total::create();
            $total->created_at = request()->get('date');
            $total->end_desc = $request->get('end_desc');
            $total->save();
            return response($total);
        }
    }
}

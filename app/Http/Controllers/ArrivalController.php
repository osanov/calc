<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreArrivalRequest;
use App\Http\Requests\UpdateArrivalRequest;
use App\Models\Arrival;

class ArrivalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->get('perPage')) {
            return response(Arrival::paginate(request()->get('perPage', 10)));
        } else {
            return \App\Http\Resources\Arrival\ArrivalResource::collection(Arrival::when(request()->get('date'), function($q) {
                return $q->whereDate('created_at', request()->get('date'));
            }, function($q) {
                return $q->whereDate('created_at', Carbon::today());
            })->with(['arrivalType'])->get());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreArrivalRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArrivalRequest $request)
    {
        $arrival = Arrival::create($request->post());
        if ($request->has('date')) {
            $arrival->created_at = request()->get('date');
            $arrival->save();
        }
        return response($arrival);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Arrival  $arrival
     * @return \Illuminate\Http\Response
     */
    public function show(Arrival $arrival)
    {
        return response($arrival);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateArrivalRequest  $request
     * @param  \App\Models\Arrival  $arrival
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArrivalRequest $request, Arrival $arrival)
    {
        return response($arrival->update($request->post()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Arrival  $arrival
     * @return \Illuminate\Http\Response
     */
    public function destroy(Arrival $arrival)
    {
        $arrival->delete();
        return response()->json(['status' => 'ok']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Expense;
use Carbon\Carbon;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = City::with(['expenses' => function($q) {
            return $q->when(request()->get('date'), function($q) {
                $date = request()->get('date');
                return $q->whereDate('created_at', $date);
            }, function($q) {
                return $q->whereDate('created_at', Carbon::today());
            })->when(request()->get('desc'), function($q) {
                return $q->where('desc', 'LIKE', '%'.request()->get('desc').'%');
            })->when(request()->has('projects'), function($q) {
                return $q->whereIn('project_id', request()->get('projects'));
            })->when(request()->has('partners'), function($q) {
                return $q->whereIn('partner_id', request()->get('partners'));
            })->when(request()->get('city'), function($q) {
                return $q->where('city_id', request()->get('city'));
            })->when(request()->get('payment_types'), function($q) {
                return $q->where('payment_type_id', request()->get('payment_types'));
            })->when(request()->get('payment_methods'), function($q) {
                return $q->where('payment_method_id', request()->get('payment_methods'));
            });
        }, 'expenses.partner', 'expenses.project', 'expenses.paymentType', 'expenses.paymentMethod'])->get();
        return response($data);
    }
}

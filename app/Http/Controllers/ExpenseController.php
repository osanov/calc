<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreExpenseRequest;
use App\Http\Requests\UpdateExpenseRequest;
use App\Models\Expense;
use Carbon\Carbon;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->get('perPage')) {
            return response(Expense::
                when(request()->get('date'), function($q) {
                    $date = request()->get('date');
                    return count($date) > 1 ? $q->whereDate('created_at', '>=', $date[0])->whereDate('created_at', '<=', $date[1]) : $q->whereDate('created_at', $date[0]);
                }, function($q) {
                    return $q->whereDate('created_at', Carbon::today());
                })->when(request()->get('desc'), function($q) {
                    return $q->where(function($q) {
                        return $q->where('desc', 'LIKE', '%'.request()->get('desc').'%')
                        ->orWhere('value', 'LIKE', request()->get('desc').'%');
                    });
                })->when(request()->has('projects'), function($q) {
                    return $q->whereIn('project_id', request()->get('projects'));
                })->when(request()->has('partners'), function($q) {
                    return $q->whereIn('partner_id', request()->get('partners'));
                })->when(request()->get('cities'), function($q) {
                    return $q->whereIn('city_id', request()->get('cities'));
                })->when(request()->has('payment_methods'), function($q) {
                    return $q->whereIn('payment_method_id', request()->get('payment_methods'));
                })->when(request()->get('payment_types'), function($q) {
                    return $q->whereIn('payment_type_id', request()->get('payment_types'));
                })
                ->with(request()->get('with') ?? [])
                ->paginate(request()->get('perPage', 10)));
        } else {
            return \App\Http\Resources\Expense\ExpenseResource::collection(Expense::all());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreExpenseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExpenseRequest $request)
    {
        $expense = Expense::create($request->post());
        if ($request->has('date')) {
            $expense->created_at = request()->get('date');
            $expense->save();
        }
        return response($expense);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        return response($expense);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateExpenseRequest  $request
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExpenseRequest $request, Expense $expense)
    {
        return response($expense->update($request->post()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
        return response()->json(['status' => 'ok']);
    }
}

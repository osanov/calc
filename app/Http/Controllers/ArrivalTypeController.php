<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreArrivalTypeRequest;
use App\Http\Requests\UpdateArrivalTypeRequest;
use App\Models\ArrivalType;

class ArrivalTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->get('perPage')) {
            return response(ArrivalType::paginate(request()->get('perPage', 10)));
        } else {
            return \App\Http\Resources\ArrivalType\ArrivalTypeResource::collection(ArrivalType::all());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreArrivalTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArrivalTypeRequest $request)
    {
        return response(ArrivalType::create($request->post()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArrivalType  $arrivalType
     * @return \Illuminate\Http\Response
     */
    public function show(ArrivalType $arrivalType)
    {
        return response($arrivalType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateArrivalTypeRequest  $request
     * @param  \App\Models\ArrivalType  $arrivalType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArrivalTypeRequest $request, ArrivalType $arrivalType)
    {
        return response($arrivalType->update($request->post()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArrivalType  $arrivalType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArrivalType $arrivalType)
    {
        $arrivalType->delete();
        return response()->json(['status' => 'ok']);
    }
}

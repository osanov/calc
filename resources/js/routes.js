import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/js/pages/Home'
import Login from '@/js/pages/Login'
import Cities from '@/js/pages/Cities'
import Projects from '@/js/pages/Projects'
import Partners from '@/js/pages/Partners'
import Users from '@/js/pages/Users'
import Expenses from '@/js/pages/Expenses'
import PaymentTypes from '@/js/pages/PaymentTypes'
import PaymentMethods from '@/js/pages/PaymentMethods'
import ArrivalTypes from '@/js/pages/ArrivalTypes'
import Search from '@/js/pages/Search'


Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/cities',
      name: 'cities',
      component: Cities,
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects,
    },
    {
      path: '/partners',
      name: 'partners',
      component: Partners,
    },
    {
      path: '/users',
      name: 'users',
      component: Users,
    },
    {
      path: '/expenses/:city?',
      name: 'expenses',
      component: Expenses,
    },
    {
      path: '/search',
      name: 'search',
      component: Search,
    },
    {
      path: '/payment-types',
      name: 'paymentTypes',
      component: PaymentTypes,
    },
    {
      path: '/payment-methods',
      name: 'paymentMethods',
      component: PaymentMethods,
    },
    {
      path: '/arrival-types',
      name: 'arrivalTypes',
      component: ArrivalTypes,
    },
    {
      path: '/login-vue',
      name: 'login',
      component: Login,
      meta: {
        hideNavbar: true,
      }
    },
    {
      path: '/price/:price',
      name: 'price',
      component: Home,
    }
  ]
});

export default router;
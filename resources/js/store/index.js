import Vue from 'vue'
import Vuex from 'vuex'
import total from './modules/total'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    total
  }
})
import axios from "axios"

const state = {
  total: {
    rest: 0,
    arrival: 0,
    open: 0,
  },
  currentDate: null,
}

const getters = {
  TOTAL: state => state.total,
  CURRENT_DATE: state => state.currentDate,
}

const mutations = {
  SET_TOTAL: (state, payload) => {
    state.total = payload
  },
  SET_CURRENT_DATE: (state, payload) => {
    state.currentDate = payload
  }
}

const actions = {
  FETCH_TOTAL: async (ctx, payload = '') => {
    let { data } = await axios.get(`/api/total?${payload}`)
    ctx.commit('SET_TOTAL', data)
  },
  ADD_TOTAL: async (ctx, payload = '') => {
    let { data } = await axios.get(`/api/add-total?${payload}`)
    ctx.commit('SET_TOTAL', data)
  },
  SUB_TOTAL: async (ctx, payload = '') => {
    let { data } = await axios.get(`/api/sub-total?${payload}`)
    ctx.commit('SET_TOTAL', data)
  },
  OPEN_TOTAL: async (ctx, payload = '') => {
    let { data } = await axios.get(`/api/open-total-desc?${payload}`)
    ctx.commit('SET_TOTAL', data)
  },
  END_TOTAL: async (ctx, payload = '') => {
    let { data } = await axios.get(`/api/end-total-desc?${payload}`)
    ctx.commit('SET_TOTAL', data)
  },
  UPDATE_CURRENT_DATE: async (ctx, payload) => {
    ctx.commit('SET_CURRENT_DATE', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
};
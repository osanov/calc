<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::apiResource('cities', 'App\Http\Controllers\CityController')->middleware('auth:sanctum');
// Route::apiResource('projects', 'App\Http\Controllers\ProjectController')->middleware('auth:sanctum');
// Route::apiResource('partners', 'App\Http\Controllers\PartnerController')->middleware('auth:sanctum');
// Route::apiResource('users', 'App\Http\Controllers\UserController')->middleware('auth:sanctum');
// Route::apiResource('expenses', 'App\Http\Controllers\ExpenseController')->middleware('auth:sanctum');
// Route::apiResource('search', 'App\Http\Controllers\SearchController')->middleware('auth:sanctum');
// Route::apiResource('payment-methods', 'App\Http\Controllers\PaymentMethodController')->middleware('auth:sanctum');
// Route::apiResource('payment-types', 'App\Http\Controllers\PaymentTypeController')->middleware('auth:sanctum');
// Route::apiResource('total', 'App\Http\Controllers\TotalController')->middleware('auth:sanctum');

Route::apiResource('cities', 'App\Http\Controllers\CityController')->middleware('auth:sanctum');
Route::apiResource('projects', 'App\Http\Controllers\ProjectController')->middleware('auth:sanctum');
Route::apiResource('partners', 'App\Http\Controllers\PartnerController')->middleware('auth:sanctum');
Route::apiResource('users', 'App\Http\Controllers\UserController')->middleware('auth:sanctum');
Route::apiResource('expenses', 'App\Http\Controllers\ExpenseController')->middleware('auth:sanctum');
Route::apiResource('search', 'App\Http\Controllers\SearchController')->middleware('auth:sanctum');
Route::apiResource('payment-methods', 'App\Http\Controllers\PaymentMethodController')->middleware('auth:sanctum');
Route::apiResource('payment-types', 'App\Http\Controllers\PaymentTypeController')->middleware('auth:sanctum');
Route::apiResource('arrivals', 'App\Http\Controllers\ArrivalController')->middleware('auth:sanctum');
Route::apiResource('arrival-types', 'App\Http\Controllers\ArrivalTypeController')->middleware('auth:sanctum');
// Route::apiResource('total', 'App\Http\Controllers\TotalController')->middleware('auth:sanctum');
Route::get('total', 'App\Http\Controllers\TotalController@getTotal')->middleware('auth:sanctum');
Route::post('total', 'App\Http\Controllers\TotalController@updateTotal')->middleware('auth:sanctum');
Route::post('add-total', 'App\Http\Controllers\TotalController@addTotal')->middleware('auth:sanctum');
Route::post('sub-total', 'App\Http\Controllers\TotalController@subTotal')->middleware('auth:sanctum');
Route::post('open-total-desc', 'App\Http\Controllers\TotalController@updateOpenDesc')->middleware('auth:sanctum');
Route::post('end-total-desc', 'App\Http\Controllers\TotalController@updateEndDesc')->middleware('auth:sanctum');

// App\Models\User::create(["name"=>"admin","email"=>"admin@admin.admin","password"=>\Illuminate\Support\Facades\Hash::make("12345678"),"is_admin"=>true])

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

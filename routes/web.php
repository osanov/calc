<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{any}', function () {
//     return view('app');
// })->where('any', '.*');


Route::get('/', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/cities', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/projects', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/partners', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/users', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/expenses', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/expenses/{any}', function () {return view('app');})->where('any', '.*')->middleware('auth:sanctum');
Route::get('/payment-types', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/payment-methods', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/arrival-types', function () {return view('app');})->middleware('auth:sanctum');
Route::get('/search', function () {return view('app');})->middleware('auth:sanctum');

Route::get('/login-vue', function () {return view('app');});

Auth::routes();

